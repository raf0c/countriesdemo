package com.example.rafaelgarcia.countriesdemo;

import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

	private static final String TAG = MainActivity.class.getSimpleName();
	private static final String JAVASCRIPT_INTERFACE_OBJECT = "Android";
	private static final String COUNTRIES_HTML = "file:///android_asset/Countries.html";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final WebView myWebView = (WebView) findViewById(R.id.webview);
		myWebView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.addJavascriptInterface(new WebAppInterface(this), JAVASCRIPT_INTERFACE_OBJECT);
		myWebView.loadUrl(COUNTRIES_HTML);

		makeAppDebuggableOnChrome();


		Button firstCountryButton = (Button) findViewById(R.id.first_country_id);
		firstCountryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				myWebView.loadUrl("javascript: selectCountry('MX')");
			}
		});
		Button secondCountryButton = (Button) findViewById(R.id.second_country_id);
		secondCountryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				myWebView.loadUrl("javascript: selectCountry('SE')");
			}
		});

	}

	private void makeAppDebuggableOnChrome() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
				WebView.setWebContentsDebuggingEnabled(true);
			}
		}
	}
}
